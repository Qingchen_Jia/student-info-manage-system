# Student Info Manage System

#### 介绍
A system for managing basic student information.

#### 软件架构
The original `JDBC` technology is used to control the `MySQL` database to add, delete and modify data.

#### 使用说明
You need to download the `MySQL` database locally and change the user and passwd parameters in the `ConnectMysql.java` file to the local account information.
You can quickly create the same database structure by importing SQL scripts.

```shell
cd ./data
mysql -u root -p
source init.sql
```

