package StuInfoManageSystem;

import java.sql.*;

public class StudentSystem {
    private static final Statement STATEMENT;

    static {
        try {
            STATEMENT = new ConnectMysql().getStatement();
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void deleteStudent(String id) throws SQLException {
        //从数据库中删除
        String sql = "DELETE FROM studentinfo.student WHERE Id='%s';";
        String worksql = String.format(sql, id);
        STATEMENT.executeUpdate(worksql);
    }

    public static boolean contains(String id) throws SQLException {
        return getStudent(id).next();
    }

    public static ResultSet getStudent(String id) throws SQLException {
        String sql = "SELECT * FROM studentinfo.student WHERE Id='%s';";
        String worksql = String.format(sql, id);
        return STATEMENT.executeQuery(worksql);
    }

    public static ResultSet getAllStudent() throws SQLException {
        String sql = "SELECT * FROM studentinfo.student;";
        return STATEMENT.executeQuery(sql);
    }

    public static void addStudent(Student student) throws SQLException {
        //添加信息到数据库
        String sql = "INSERT INTO studentinfo.student VALUES ('%s','%s','%s');";
        String worksql = String.format(sql, student.getId(), student.getName(), student.getMajor());
        STATEMENT.executeUpdate(worksql);
    }
}
