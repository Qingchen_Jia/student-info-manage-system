package StuInfoManageSystem;

import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

public class UserSystem {
    private static final Statement STATEMENT;

    static {
        try {
            STATEMENT = new ConnectMysql().getStatement();
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public UserSystem() throws SQLException, ClassNotFoundException {
    }

    public static int cheekUsername(String str) throws SQLException {
        //用户名唯一
        if (containsUser(str)) {
            return 1;
        }

        //包含大小写字母和数字，且不能为纯数字
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isLowerCase(str.charAt(i)) && !Character.isUpperCase(str.charAt(i)) && !Character.isDigit(str.charAt(i))) {
                return 2;
            }
        }
        boolean flag = false;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isUpperCase(str.charAt(i)) || (Character.isLowerCase(str.charAt(i)))) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            return 2;
        }
        return 0;
    }

    public static boolean cheekPersonID(String str) {
        //长度为18位，前17位均为数字，末位可为数字或‘X’或'x'
        return str.matches("[1-9][0-9]{16}[0-9xX]");
    }

    public static boolean cheekPhoneNum(String str) {
        //长度为11位，必须是'1'开头，全部为数字
        return str.matches("[1][0-9]{10}");
    }

    public static void addUser(User user) throws SQLException {
        String sql = "INSERT INTO studentinfo.user VALUES ('%s','%s','%s','%s');";
        String worksql = String.format(sql, user.getUsername(), user.getPassword(), user.getPersonID(), user.getPhoneNum());
        STATEMENT.executeUpdate(worksql);
    }

    public static ResultSet getUser(String username) throws SQLException {
        String sql = "SELECT * FROM studentinfo.user WHERE username='%s';";
        String worksql = String.format(sql, username);
        return STATEMENT.executeQuery(worksql);
    }

    public static boolean containsUser(String username) throws SQLException {
        return getUser(username).next();
    }

    public static String getUserPassword(String username) throws SQLException {
        ResultSet resultSet = getUser(username);
        resultSet.next();
        return resultSet.getString("password");
    }

    public static String getUserPersonId(String username) throws SQLException {
        ResultSet resultSet = getUser(username);
        resultSet.next();
        return resultSet.getString("personId");
    }

    public static String getUserPhoneNum(String username) throws SQLException {
        ResultSet resultSet = getUser(username);
        resultSet.next();
        return resultSet.getString("phoneNum");
    }

    public static void updateUserPassword(String username, String password) throws SQLException {
        String sql = "UPDATE studentinfo.user SET password='%s' WHERE username='%s';";
        String worksql = String.format(sql, password, username);
        STATEMENT.executeUpdate(worksql);
    }

    public static String getCode() {
        //长度为5，4位大小写字母和1位数字，位置随机
        ArrayList<Character> characters = new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            characters.add((char) ('a' + i));
        }
        for (int i = 0; i < 26; i++) {
            characters.add((char) ('A' + i));
        }
        Random index = new Random();
        StringBuffer strb = new StringBuffer();
        for (int i = 0; i < 4; i++) {
            strb.append(characters.get(index.nextInt(characters.size())));
        }
        strb.append(index.nextInt(10));
        char[] arr = strb.toString().toCharArray();
        int exchangeIndex = index.nextInt(arr.length);
        char temp = arr[exchangeIndex];
        arr[exchangeIndex] = arr[arr.length - 1];
        arr[arr.length - 1] = temp;
        return new String(arr);
    }
}