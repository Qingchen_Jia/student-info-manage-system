package StuInfoManageSystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectMysql {
    public Statement statement;

    public ConnectMysql() throws ClassNotFoundException, SQLException {
        //连接MySQL数据库
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://127.0.0.1:3306";
        String user = "root";
        String passwd = "jqc004118";
        Connection connection = DriverManager.getConnection(url, user, passwd);
        statement = connection.createStatement();
    }

    public Statement getStatement() {
        return statement;
    }
}
