/*
 * Created by JFormDesigner on Wed Apr 03 14:19:02 CST 2024
 */

package UI;

import StuInfoManageSystem.UserSystem;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;

/**
 * @author 87948
 */
public class LoginFrame extends JFrame {
    public LoginFrame() {
        initComponents();
        this.setVisible(true);
    }

    private void rightcodeMouseReleased(MouseEvent e) {
        // TODO add your code here
        String str = UserSystem.getCode();
        rightcode.setText(str);
    }

    private void loginMouseReleased(MouseEvent e) throws SQLException, ClassNotFoundException {
        // TODO add your code here
        userLogin(inputusername.getText(), new String(inputpassword.getPassword()), inputcode.getText());
    }

    private void registerMouseReleased(MouseEvent e) {
        // TODO add your code here
        new RegisterFrame();
        this.dispose();
    }

    private void findpasswordMouseReleased(MouseEvent e) {
        // TODO add your code here
        new FindPasswdFrame();
        this.dispose();
    }

    private void enterMouseReleased(MouseEvent e) throws SQLException {
        // TODO add your code here
        new StudentInfoFrame();
        okdialog.dispose();
        this.dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        Title = new JLabel();
        usrname = new JLabel();
        password = new JLabel();
        inputusername = new JTextField();
        inputpassword = new JPasswordField();
        code = new JLabel();
        rightcode = new JLabel();
        login = new JButton();
        register = new JButton();
        findpassword = new JButton();
        inputcode = new JTextField();
        nouserdialog = new JDialog();
        nousertext = new JLabel();
        errorcodedialog = new JDialog();
        errcodetext = new JLabel();
        wrongpassworddialog = new JDialog();
        wrongpasswordtext = new JLabel();
        okdialog = new JDialog();
        enter = new JButton();

        //======== this ========
        setFont(new Font(Font.DIALOG, Font.PLAIN, 14));
        setAlwaysOnTop(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- Title ----
        Title.setText("\u5b66\u751f\u4fe1\u606f\u7ba1\u7406\u7cfb\u7edf");
        Title.setFont(Title.getFont().deriveFont(Title.getFont().getStyle() & ~Font.ITALIC, Title.getFont().getSize() + 15f));
        contentPane.add(Title);
        Title.setBounds(new Rectangle(new Point(130, 40), Title.getPreferredSize()));

        //---- usrname ----
        usrname.setText("\u7528\u6237\u540d\uff1a");
        usrname.setFont(usrname.getFont().deriveFont(usrname.getFont().getSize() + 6f));
        contentPane.add(usrname);
        usrname.setBounds(new Rectangle(new Point(70, 95), usrname.getPreferredSize()));

        //---- password ----
        password.setText("\u5bc6\u7801\uff1a");
        password.setFont(password.getFont().deriveFont(password.getFont().getSize() + 6f));
        contentPane.add(password);
        password.setBounds(new Rectangle(new Point(85, 135), password.getPreferredSize()));
        contentPane.add(inputusername);
        inputusername.setBounds(145, 95, 230, 25);
        contentPane.add(inputpassword);
        inputpassword.setBounds(145, 135, 230, 25);

        //---- code ----
        code.setText("\u9a8c\u8bc1\u7801\uff1a");
        code.setFont(code.getFont().deriveFont(code.getFont().getSize() + 6f));
        contentPane.add(code);
        code.setBounds(new Rectangle(new Point(70, 175), code.getPreferredSize()));

        //---- rightcode ----
        rightcode.setText("\u70b9\u6211\uff01");
        rightcode.setFont(rightcode.getFont().deriveFont(rightcode.getFont().getSize() + 3f));
        rightcode.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                rightcodeMouseReleased(e);
            }
        });
        contentPane.add(rightcode);
        rightcode.setBounds(220, 180, 60, 20);

        //---- login ----
        login.setText("\u767b\u5f55");
        login.setFont(login.getFont().deriveFont(Font.BOLD, login.getFont().getSize() + 6f));
        login.setFocusPainted(false);
        login.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                try {
loginMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
} catch (ClassNotFoundException ex) {
    throw new RuntimeException(ex);
}
            }
        });
        contentPane.add(login);
        login.setBounds(new Rectangle(new Point(145, 215), login.getPreferredSize()));

        //---- register ----
        register.setText("\u6ce8\u518c");
        register.setFont(register.getFont().deriveFont(Font.BOLD, register.getFont().getSize() + 6f));
        register.setFocusPainted(false);
        register.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                registerMouseReleased(e);
            }
        });
        contentPane.add(register);
        register.setBounds(new Rectangle(new Point(295, 215), register.getPreferredSize()));

        //---- findpassword ----
        findpassword.setText("\u5fd8\u8bb0\u5bc6\u7801");
        findpassword.setFont(findpassword.getFont().deriveFont(Font.BOLD));
        findpassword.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                findpasswordMouseReleased(e);
            }
        });
        contentPane.add(findpassword);
        findpassword.setBounds(380, 135, findpassword.getPreferredSize().width, 25);
        contentPane.add(inputcode);
        inputcode.setBounds(145, 175, 65, 25);

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(520, 330);
        setLocationRelativeTo(getOwner());

        //======== nouserdialog ========
        {
            nouserdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            nouserdialog.setAlwaysOnTop(true);
            nouserdialog.setModal(true);
            var nouserdialogContentPane = nouserdialog.getContentPane();
            nouserdialogContentPane.setLayout(null);

            //---- nousertext ----
            nousertext.setText("\u5f53\u524d\u7528\u6237\u4e0d\u5b58\u5728\uff01\u8bf7\u5148\u6ce8\u518c");
            nousertext.setFont(nousertext.getFont().deriveFont(nousertext.getFont().getSize() + 7f));
            nouserdialogContentPane.add(nousertext);
            nousertext.setBounds(new Rectangle(new Point(45, 35), nousertext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < nouserdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = nouserdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = nouserdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                nouserdialogContentPane.setMinimumSize(preferredSize);
                nouserdialogContentPane.setPreferredSize(preferredSize);
            }
            nouserdialog.setSize(345, 130);
            nouserdialog.setLocationRelativeTo(nouserdialog.getOwner());
        }

        //======== errorcodedialog ========
        {
            errorcodedialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            errorcodedialog.setAlwaysOnTop(true);
            errorcodedialog.setModal(true);
            var errorcodedialogContentPane = errorcodedialog.getContentPane();
            errorcodedialogContentPane.setLayout(null);

            //---- errcodetext ----
            errcodetext.setText("\u9a8c\u8bc1\u7801\u4e0d\u5339\u914d\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            errcodetext.setFont(errcodetext.getFont().deriveFont(errcodetext.getFont().getSize() + 7f));
            errorcodedialogContentPane.add(errcodetext);
            errcodetext.setBounds(new Rectangle(new Point(45, 35), errcodetext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < errorcodedialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = errorcodedialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = errorcodedialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                errorcodedialogContentPane.setMinimumSize(preferredSize);
                errorcodedialogContentPane.setPreferredSize(preferredSize);
            }
            errorcodedialog.setSize(345, 130);
            errorcodedialog.setLocationRelativeTo(errorcodedialog.getOwner());
        }

        //======== wrongpassworddialog ========
        {
            wrongpassworddialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            wrongpassworddialog.setAlwaysOnTop(true);
            wrongpassworddialog.setModal(true);
            var wrongpassworddialogContentPane = wrongpassworddialog.getContentPane();
            wrongpassworddialogContentPane.setLayout(null);

            //---- wrongpasswordtext ----
            wrongpasswordtext.setText("\u5bc6\u7801\u8f93\u5165\u9519\u8bef\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            wrongpasswordtext.setFont(wrongpasswordtext.getFont().deriveFont(wrongpasswordtext.getFont().getSize() + 7f));
            wrongpassworddialogContentPane.add(wrongpasswordtext);
            wrongpasswordtext.setBounds(new Rectangle(new Point(45, 35), wrongpasswordtext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < wrongpassworddialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = wrongpassworddialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = wrongpassworddialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                wrongpassworddialogContentPane.setMinimumSize(preferredSize);
                wrongpassworddialogContentPane.setPreferredSize(preferredSize);
            }
            wrongpassworddialog.setSize(345, 130);
            wrongpassworddialog.setLocationRelativeTo(wrongpassworddialog.getOwner());
        }

        //======== okdialog ========
        {
            okdialog.setAlwaysOnTop(true);
            okdialog.setModal(true);
            okdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            var okdialogContentPane = okdialog.getContentPane();
            okdialogContentPane.setLayout(null);

            //---- enter ----
            enter.setText("\u767b\u9646\u6210\u529f\uff01\u8fdb\u5165\u7cfb\u7edf");
            enter.setFont(enter.getFont().deriveFont(enter.getFont().getSize() + 7f));
            enter.setFocusPainted(false);
            enter.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    try {
enterMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
}
                }
            });
            okdialogContentPane.add(enter);
            enter.setBounds(new Rectangle(new Point(55, 35), enter.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < okdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = okdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = okdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                okdialogContentPane.setMinimumSize(preferredSize);
                okdialogContentPane.setPreferredSize(preferredSize);
            }
            okdialog.setSize(345, 130);
            okdialog.setLocationRelativeTo(okdialog.getOwner());
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JLabel Title;
    private JLabel usrname;
    private JLabel password;
    private JTextField inputusername;
    private JPasswordField inputpassword;
    private JLabel code;
    private JLabel rightcode;
    private JButton login;
    private JButton register;
    private JButton findpassword;
    private JTextField inputcode;
    private JDialog nouserdialog;
    private JLabel nousertext;
    private JDialog errorcodedialog;
    private JLabel errcodetext;
    private JDialog wrongpassworddialog;
    private JLabel wrongpasswordtext;
    private JDialog okdialog;
    private JButton enter;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on

    public void userLogin(String usernameIn, String passwordIn, String codeIn) throws SQLException, ClassNotFoundException {
        //判断是否存在此用户名的用户
        if (!UserSystem.containsUser(usernameIn)) {
            nouserdialog.setVisible(true);
            return;
        }

        //输入验证码验证
        if (!codeIn.equalsIgnoreCase(rightcode.getText())) {
            errorcodedialog.setVisible(true);
            return;
        }

        //判断密码对错
        if (!UserSystem.getUserPassword(usernameIn).equals(passwordIn)) {
            wrongpassworddialog.setVisible(true);
            return;
        }

        okdialog.setVisible(true);
    }
}
