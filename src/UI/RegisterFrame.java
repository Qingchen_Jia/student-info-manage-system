/*
 * Created by JFormDesigner on Wed Apr 03 21:10:31 CST 2024
 */

package UI;

import java.awt.event.*;

import StuInfoManageSystem.User;
import StuInfoManageSystem.UserSystem;

import java.awt.*;
import java.sql.SQLException;
import javax.swing.*;

/**
 * @author 87948
 */
public class RegisterFrame extends JFrame {
    public RegisterFrame() {
        initComponents();
        this.setVisible(true);
    }

    private void checkusernameMouseReleased(MouseEvent e) throws SQLException {
        // TODO add your code here
        //判断用户名是否可用
        switch (UserSystem.cheekUsername(inputusername.getText())) {
            case 0 -> {
                ;
            }
            case 1 -> existusernamedialog.setVisible(true);
            case 2 -> errorusernamedialog.setVisible(true);
        }
    }

    private void checkpasswordMouseReleased(MouseEvent e) {
        // TODO add your code here
        //判断密码是否确认
        if (!new String(inputpasswrodagain.getPassword()).equals(new String(inputpassword.getPassword()))) {
            differentpassworddialog.setVisible(true);
        }
    }

    private void checkpersonidMouseReleased(MouseEvent e) {
        // TODO add your code here
        //判断身份证格式
        if (!UserSystem.cheekPersonID(inputpersonid.getText())) {
            errorpersoniddialog.setVisible(true);
        }
    }

    private void checkphonenumMouseReleased(MouseEvent e) {
        // TODO add your code here
        //判断手机号格式
        if (!UserSystem.cheekPhoneNum(inputphonenum.getText())) {
            errorphonenumdialog.setVisible(true);
        }
    }

    private void registerMouseReleased(MouseEvent e) throws SQLException {
        // TODO add your code here
        userRegister(inputusername.getText(), new String(inputpassword.getPassword()), new String(inputpasswrodagain.getPassword()), inputpersonid.getText(), inputphonenum.getText());
    }

    private void exitMouseReleased(MouseEvent e) {
        // TODO add your code here
        new LoginFrame();
        this.dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        Title = new JLabel();
        usrname = new JLabel();
        password = new JLabel();
        inputusername = new JTextField();
        inputpassword = new JPasswordField();
        register = new JButton();
        personid = new JLabel();
        passwordagain = new JLabel();
        inputpasswrodagain = new JPasswordField();
        inputpersonid = new JTextField();
        phonenum = new JLabel();
        inputphonenum = new JTextField();
        checkusername = new JButton();
        checkpersonid = new JButton();
        checkphonenum = new JButton();
        checkpassword = new JButton();
        exit = new JButton();
        existusernamedialog = new JDialog();
        existusernametext = new JLabel();
        errorusernamedialog = new JDialog();
        errorusernametext = new JLabel();
        differentpassworddialog = new JDialog();
        differentpasswordtext = new JLabel();
        errorpersoniddialog = new JDialog();
        errorpersonidtext = new JLabel();
        errorphonenumdialog = new JDialog();
        errorphonenumtext = new JLabel();
        okdialog = new JDialog();
        oktext = new JLabel();

        //======== this ========
        setFont(new Font(Font.DIALOG, Font.PLAIN, 14));
        setAlwaysOnTop(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- Title ----
        Title.setText("\u5b66\u751f\u4fe1\u606f\u7ba1\u7406\u7cfb\u7edf");
        Title.setFont(Title.getFont().deriveFont(Title.getFont().getStyle() & ~Font.ITALIC, Title.getFont().getSize() + 15f));
        contentPane.add(Title);
        Title.setBounds(new Rectangle(new Point(130, 40), Title.getPreferredSize()));

        //---- usrname ----
        usrname.setText("\u7528\u6237\u540d\uff1a");
        usrname.setFont(usrname.getFont().deriveFont(usrname.getFont().getSize() + 6f));
        contentPane.add(usrname);
        usrname.setBounds(new Rectangle(new Point(70, 95), usrname.getPreferredSize()));

        //---- password ----
        password.setText("\u5bc6\u7801\uff1a");
        password.setFont(password.getFont().deriveFont(password.getFont().getSize() + 6f));
        contentPane.add(password);
        password.setBounds(new Rectangle(new Point(85, 135), password.getPreferredSize()));
        contentPane.add(inputusername);
        inputusername.setBounds(145, 95, 230, 25);
        contentPane.add(inputpassword);
        inputpassword.setBounds(145, 135, 230, 25);

        //---- register ----
        register.setText("\u6ce8\u518c");
        register.setFont(register.getFont().deriveFont(Font.BOLD, register.getFont().getSize() + 6f));
        register.setFocusPainted(false);
        register.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                try {
registerMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
}
            }
        });
        contentPane.add(register);
        register.setBounds(new Rectangle(new Point(220, 310), register.getPreferredSize()));

        //---- personid ----
        personid.setText("\u8eab\u4efd\u8bc1\u53f7\uff1a");
        personid.setFont(personid.getFont().deriveFont(personid.getFont().getSize() + 6f));
        contentPane.add(personid);
        personid.setBounds(new Rectangle(new Point(50, 215), personid.getPreferredSize()));

        //---- passwordagain ----
        passwordagain.setText("\u786e\u8ba4\u5bc6\u7801\uff1a");
        passwordagain.setFont(passwordagain.getFont().deriveFont(passwordagain.getFont().getSize() + 6f));
        contentPane.add(passwordagain);
        passwordagain.setBounds(new Rectangle(new Point(50, 175), passwordagain.getPreferredSize()));
        contentPane.add(inputpasswrodagain);
        inputpasswrodagain.setBounds(145, 175, 230, 25);
        contentPane.add(inputpersonid);
        inputpersonid.setBounds(145, 215, 230, 25);

        //---- phonenum ----
        phonenum.setText("\u624b\u673a\u53f7\uff1a");
        phonenum.setFont(phonenum.getFont().deriveFont(phonenum.getFont().getSize() + 6f));
        contentPane.add(phonenum);
        phonenum.setBounds(new Rectangle(new Point(65, 255), phonenum.getPreferredSize()));
        contentPane.add(inputphonenum);
        inputphonenum.setBounds(145, 255, 230, 25);

        //---- checkusername ----
        checkusername.setText("\u68c0\u9a8c");
        checkusername.setFont(checkusername.getFont().deriveFont(checkusername.getFont().getStyle() | Font.BOLD));
        checkusername.setFocusPainted(false);
        checkusername.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                try {
checkusernameMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
}
            }
        });
        contentPane.add(checkusername);
        checkusername.setBounds(395, 95, checkusername.getPreferredSize().width, 25);

        //---- checkpersonid ----
        checkpersonid.setText("\u68c0\u9a8c");
        checkpersonid.setFont(checkpersonid.getFont().deriveFont(checkpersonid.getFont().getStyle() | Font.BOLD));
        checkpersonid.setFocusPainted(false);
        checkpersonid.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                checkpersonidMouseReleased(e);
            }
        });
        contentPane.add(checkpersonid);
        checkpersonid.setBounds(395, 215, checkpersonid.getPreferredSize().width, 25);

        //---- checkphonenum ----
        checkphonenum.setText("\u68c0\u9a8c");
        checkphonenum.setFont(checkphonenum.getFont().deriveFont(checkphonenum.getFont().getStyle() | Font.BOLD));
        checkphonenum.setFocusPainted(false);
        checkphonenum.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                checkphonenumMouseReleased(e);
            }
        });
        contentPane.add(checkphonenum);
        checkphonenum.setBounds(395, 255, checkphonenum.getPreferredSize().width, 25);

        //---- checkpassword ----
        checkpassword.setText("\u68c0\u9a8c");
        checkpassword.setFont(checkpassword.getFont().deriveFont(checkpassword.getFont().getStyle() | Font.BOLD));
        checkpassword.setFocusPainted(false);
        checkpassword.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                checkpasswordMouseReleased(e);
            }
        });
        contentPane.add(checkpassword);
        checkpassword.setBounds(395, 175, checkpassword.getPreferredSize().width, 25);

        //---- exit ----
        exit.setText("\u8fd4\u56de");
        exit.setFont(exit.getFont().deriveFont(Font.BOLD, exit.getFont().getSize() + 6f));
        exit.setFocusPainted(false);
        exit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                exitMouseReleased(e);
            }
        });
        contentPane.add(exit);
        exit.setBounds(new Rectangle(new Point(220, 350), exit.getPreferredSize()));

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(520, 435);
        setLocationRelativeTo(getOwner());

        //======== existusernamedialog ========
        {
            existusernamedialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            existusernamedialog.setAlwaysOnTop(true);
            existusernamedialog.setModal(true);
            var existusernamedialogContentPane = existusernamedialog.getContentPane();
            existusernamedialogContentPane.setLayout(null);

            //---- existusernametext ----
            existusernametext.setText("\u7528\u6237\u540d\u5df2\u88ab\u5360\u7528\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            existusernametext.setFont(existusernametext.getFont().deriveFont(existusernametext.getFont().getSize() + 7f));
            existusernamedialogContentPane.add(existusernametext);
            existusernametext.setBounds(new Rectangle(new Point(45, 35), existusernametext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < existusernamedialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = existusernamedialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = existusernamedialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                existusernamedialogContentPane.setMinimumSize(preferredSize);
                existusernamedialogContentPane.setPreferredSize(preferredSize);
            }
            existusernamedialog.setSize(345, 130);
            existusernamedialog.setLocationRelativeTo(existusernamedialog.getOwner());
        }

        //======== errorusernamedialog ========
        {
            errorusernamedialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            errorusernamedialog.setAlwaysOnTop(true);
            errorusernamedialog.setModal(true);
            var errorusernamedialogContentPane = errorusernamedialog.getContentPane();
            errorusernamedialogContentPane.setLayout(null);

            //---- errorusernametext ----
            errorusernametext.setText("\u7528\u6237\u540d\u683c\u5f0f\u9519\u8bef\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            errorusernametext.setFont(errorusernametext.getFont().deriveFont(errorusernametext.getFont().getSize() + 7f));
            errorusernamedialogContentPane.add(errorusernametext);
            errorusernametext.setBounds(new Rectangle(new Point(45, 35), errorusernametext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < errorusernamedialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = errorusernamedialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = errorusernamedialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                errorusernamedialogContentPane.setMinimumSize(preferredSize);
                errorusernamedialogContentPane.setPreferredSize(preferredSize);
            }
            errorusernamedialog.setSize(345, 130);
            errorusernamedialog.setLocationRelativeTo(errorusernamedialog.getOwner());
        }

        //======== differentpassworddialog ========
        {
            differentpassworddialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            differentpassworddialog.setAlwaysOnTop(true);
            differentpassworddialog.setModal(true);
            var differentpassworddialogContentPane = differentpassworddialog.getContentPane();
            differentpassworddialogContentPane.setLayout(null);

            //---- differentpasswordtext ----
            differentpasswordtext.setText("\u4e24\u6b21\u5bc6\u7801\u4e0d\u4e00\u81f4\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            differentpasswordtext.setFont(differentpasswordtext.getFont().deriveFont(differentpasswordtext.getFont().getSize() + 7f));
            differentpassworddialogContentPane.add(differentpasswordtext);
            differentpasswordtext.setBounds(new Rectangle(new Point(45, 35), differentpasswordtext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < differentpassworddialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = differentpassworddialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = differentpassworddialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                differentpassworddialogContentPane.setMinimumSize(preferredSize);
                differentpassworddialogContentPane.setPreferredSize(preferredSize);
            }
            differentpassworddialog.setSize(345, 130);
            differentpassworddialog.setLocationRelativeTo(differentpassworddialog.getOwner());
        }

        //======== errorpersoniddialog ========
        {
            errorpersoniddialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            errorpersoniddialog.setAlwaysOnTop(true);
            errorpersoniddialog.setModal(true);
            var errorpersoniddialogContentPane = errorpersoniddialog.getContentPane();
            errorpersoniddialogContentPane.setLayout(null);

            //---- errorpersonidtext ----
            errorpersonidtext.setText("\u8eab\u4efd\u8bc1\u683c\u5f0f\u9519\u8bef\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            errorpersonidtext.setFont(errorpersonidtext.getFont().deriveFont(errorpersonidtext.getFont().getSize() + 7f));
            errorpersoniddialogContentPane.add(errorpersonidtext);
            errorpersonidtext.setBounds(new Rectangle(new Point(45, 35), errorpersonidtext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < errorpersoniddialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = errorpersoniddialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = errorpersoniddialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                errorpersoniddialogContentPane.setMinimumSize(preferredSize);
                errorpersoniddialogContentPane.setPreferredSize(preferredSize);
            }
            errorpersoniddialog.setSize(345, 130);
            errorpersoniddialog.setLocationRelativeTo(errorpersoniddialog.getOwner());
        }

        //======== errorphonenumdialog ========
        {
            errorphonenumdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            errorphonenumdialog.setAlwaysOnTop(true);
            errorphonenumdialog.setModal(true);
            var errorphonenumdialogContentPane = errorphonenumdialog.getContentPane();
            errorphonenumdialogContentPane.setLayout(null);

            //---- errorphonenumtext ----
            errorphonenumtext.setText("\u624b\u673a\u53f7\u683c\u5f0f\u9519\u8bef\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            errorphonenumtext.setFont(errorphonenumtext.getFont().deriveFont(errorphonenumtext.getFont().getSize() + 7f));
            errorphonenumdialogContentPane.add(errorphonenumtext);
            errorphonenumtext.setBounds(new Rectangle(new Point(45, 35), errorphonenumtext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < errorphonenumdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = errorphonenumdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = errorphonenumdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                errorphonenumdialogContentPane.setMinimumSize(preferredSize);
                errorphonenumdialogContentPane.setPreferredSize(preferredSize);
            }
            errorphonenumdialog.setSize(345, 130);
            errorphonenumdialog.setLocationRelativeTo(errorphonenumdialog.getOwner());
        }

        //======== okdialog ========
        {
            okdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            okdialog.setAlwaysOnTop(true);
            okdialog.setModal(true);
            var okdialogContentPane = okdialog.getContentPane();
            okdialogContentPane.setLayout(null);

            //---- oktext ----
            oktext.setText("\u8d26\u6237\u6ce8\u518c\u6210\u529f\uff01\u8bf7\u8fd4\u56de\u767b\u5f55");
            oktext.setFont(oktext.getFont().deriveFont(oktext.getFont().getSize() + 7f));
            okdialogContentPane.add(oktext);
            oktext.setBounds(new Rectangle(new Point(45, 35), oktext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < okdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = okdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = okdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                okdialogContentPane.setMinimumSize(preferredSize);
                okdialogContentPane.setPreferredSize(preferredSize);
            }
            okdialog.setSize(345, 130);
            okdialog.setLocationRelativeTo(okdialog.getOwner());
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JLabel Title;
    private JLabel usrname;
    private JLabel password;
    private JTextField inputusername;
    private JPasswordField inputpassword;
    private JButton register;
    private JLabel personid;
    private JLabel passwordagain;
    private JPasswordField inputpasswrodagain;
    private JTextField inputpersonid;
    private JLabel phonenum;
    private JTextField inputphonenum;
    private JButton checkusername;
    private JButton checkpersonid;
    private JButton checkphonenum;
    private JButton checkpassword;
    private JButton exit;
    private JDialog existusernamedialog;
    private JLabel existusernametext;
    private JDialog errorusernamedialog;
    private JLabel errorusernametext;
    private JDialog differentpassworddialog;
    private JLabel differentpasswordtext;
    private JDialog errorpersoniddialog;
    private JLabel errorpersonidtext;
    private JDialog errorphonenumdialog;
    private JLabel errorphonenumtext;
    private JDialog okdialog;
    private JLabel oktext;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on

    public void userRegister(String usernameIn, String passwordIn, String passwordagainIn, String personidIn, String phonenumIn) throws SQLException {
        User user = new User();
        //判断用户名是否可用
        switch (UserSystem.cheekUsername(usernameIn)) {
            case 0 -> user.setUsername(usernameIn);
            case 1 -> {
                existusernamedialog.setVisible(true);
                return;
            }
            case 2 -> {
                errorusernamedialog.setVisible(true);
                return;
            }
        }

        //判断密码是否确认
        if (passwordagainIn.equals(passwordIn)) {
            user.setPassword(passwordagainIn);
        } else {
            differentpassworddialog.setVisible(true);
            return;
        }

        //判断身份证格式
        if (UserSystem.cheekPersonID(personidIn)) {
            user.setPersonID(personidIn);
        } else {
            errorpersoniddialog.setVisible(true);
            return;
        }

        //判断手机号格式
        if (UserSystem.cheekPhoneNum(phonenumIn)) {
            user.setPhoneNum(phonenumIn);
        } else {
            errorphonenumdialog.setVisible(true);
            return;
        }

        UserSystem.addUser(user);
        okdialog.setVisible(true);
    }
}
