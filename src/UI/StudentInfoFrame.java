/*
 * Created by JFormDesigner on Thu Apr 04 13:24:21 CST 2024
 */

package UI;

import java.awt.*;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import StuInfoManageSystem.Student;
import StuInfoManageSystem.StudentSystem;

/**
 * @author 87948
 */
public class StudentInfoFrame extends JFrame {
    public StudentInfoFrame() throws SQLException {
        initComponents();
        studentQuery();
        this.setVisible(true);
    }

    private void queryMouseReleased(MouseEvent e) throws SQLException {
        // TODO add your code here
        ResultSet resultSet = StudentSystem.getStudent(inputidquery.getText());
        //判断有无结果
        if (!resultSet.next()) {
            nostudentdialog.setVisible(true);
            return;
        } else {
            //将查询数据放入容器
            getidtext.setText(resultSet.getString(1));
            getnametext.setText(resultSet.getString(2));
            getmajortext.setText(resultSet.getString(3));
            querydialog.setVisible(true);
        }
    }

    private void addMouseReleased(MouseEvent e) {
        // TODO add your code here
        adddialog.setVisible(true);
    }

    private void exitMouseReleased(MouseEvent e) {
        // TODO add your code here
        new LoginFrame();
        this.dispose();
    }

    private void deleteMouseReleased(MouseEvent e) throws SQLException {
        // TODO add your code here
        StudentSystem.deleteStudent(inputiddelete.getText());
        deleteokdialog.setVisible(true);
    }

    private void addokbuttonMouseReleased(MouseEvent e) throws SQLException {
        // TODO add your code here
        addokdialog.dispose();
        adddialog.dispose();
        this.dispose();
        new StudentInfoFrame();
    }

    private void deleteokbuttonMouseReleased(MouseEvent e) throws SQLException {
        // TODO add your code here
        deleteokdialog.dispose();
        this.dispose();
        new StudentInfoFrame();
    }

    private void confirmbuttonMouseReleased(MouseEvent e) throws SQLException {
        // TODO add your code here
        studentAdd(addidtext.getText(), addnametext.getText(), addmajortext.getText());
        addokdialog.setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        systemname = new JLabel();
        title = new JScrollPane();
        table = new JTable();
        query = new JButton();
        add = new JButton();
        delete = new JButton();
        inputidquery = new JTextField();
        exit = new JButton();
        inputiddelete = new JTextField();
        querydialog = new JDialog();
        getid = new JLabel();
        getname = new JLabel();
        getmajor = new JLabel();
        getidtext = new JLabel();
        getnametext = new JLabel();
        getmajortext = new JLabel();
        nostudentdialog = new JDialog();
        nostudenttext = new JLabel();
        adddialog = new JDialog();
        addid = new JLabel();
        addname = new JLabel();
        addmajor = new JLabel();
        addidtext = new JTextField();
        addnametext = new JTextField();
        addmajortext = new JTextField();
        confirmbutton = new JButton();
        existstudentdialog = new JDialog();
        existstudenttext = new JLabel();
        addokdialog = new JDialog();
        addokbutton = new JButton();
        deleteokdialog = new JDialog();
        deleteokbutton = new JButton();

        //======== this ========
        setAlwaysOnTop(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- systemname ----
        systemname.setText("\u5b66\u751f\u4fe1\u606f\u7ba1\u7406\u7cfb\u7edf");
        systemname.setFont(systemname.getFont().deriveFont(systemname.getFont().getSize() + 18f));
        contentPane.add(systemname);
        systemname.setBounds(new Rectangle(new Point(215, 20), systemname.getPreferredSize()));

        //======== title ========
        {

            //---- table ----
            table.setFont(new Font("\u5b8b\u4f53", Font.PLAIN, 16));
            table.setModel(new DefaultTableModel(
                new Object[][] {
                },
                new String[] {
                    "\u5b66\u53f7", "\u59d3\u540d", "\u4e13\u4e1a"
                }
            ));
            table.setRowHeight(25);
            title.setViewportView(table);
        }
        contentPane.add(title);
        title.setBounds(0, 100, 695, 150);

        //---- query ----
        query.setText("\u67e5\u8be2");
        query.setFont(query.getFont().deriveFont(query.getFont().getSize() + 7f));
        query.setFocusPainted(false);
        query.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                try {
queryMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
}
            }
        });
        contentPane.add(query);
        query.setBounds(new Rectangle(new Point(600, 60), query.getPreferredSize()));

        //---- add ----
        add.setText("\u6dfb\u52a0");
        add.setFont(add.getFont().deriveFont(add.getFont().getSize() + 7f));
        add.setFocusPainted(false);
        add.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                addMouseReleased(e);
            }
        });
        contentPane.add(add);
        add.setBounds(new Rectangle(new Point(15, 275), add.getPreferredSize()));

        //---- delete ----
        delete.setText("\u5220\u9664");
        delete.setFont(delete.getFont().deriveFont(delete.getFont().getSize() + 7f));
        delete.setFocusPainted(false);
        delete.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                try {
deleteMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
}
            }
        });
        contentPane.add(delete);
        delete.setBounds(new Rectangle(new Point(15, 60), delete.getPreferredSize()));
        contentPane.add(inputidquery);
        inputidquery.setBounds(465, 65, 125, inputidquery.getPreferredSize().height);

        //---- exit ----
        exit.setText("\u9000\u51fa");
        exit.setFont(exit.getFont().deriveFont(exit.getFont().getSize() + 7f));
        exit.setFocusPainted(false);
        exit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                exitMouseReleased(e);
            }
        });
        contentPane.add(exit);
        exit.setBounds(new Rectangle(new Point(595, 275), exit.getPreferredSize()));
        contentPane.add(inputiddelete);
        inputiddelete.setBounds(95, 65, 125, inputiddelete.getPreferredSize().height);

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(695, 360);
        setLocationRelativeTo(getOwner());

        //======== querydialog ========
        {
            querydialog.setAlwaysOnTop(true);
            querydialog.setModal(true);
            querydialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            var querydialogContentPane = querydialog.getContentPane();
            querydialogContentPane.setLayout(null);

            //---- getid ----
            getid.setText("\u5b66\u53f7\uff1a");
            getid.setFont(getid.getFont().deriveFont(getid.getFont().getSize() + 8f));
            querydialogContentPane.add(getid);
            getid.setBounds(new Rectangle(new Point(65, 35), getid.getPreferredSize()));

            //---- getname ----
            getname.setText("\u59d3\u540d\uff1a");
            getname.setFont(getname.getFont().deriveFont(getname.getFont().getSize() + 8f));
            querydialogContentPane.add(getname);
            getname.setBounds(new Rectangle(new Point(65, 75), getname.getPreferredSize()));

            //---- getmajor ----
            getmajor.setText("\u4e13\u4e1a\uff1a");
            getmajor.setFont(getmajor.getFont().deriveFont(getmajor.getFont().getSize() + 8f));
            querydialogContentPane.add(getmajor);
            getmajor.setBounds(new Rectangle(new Point(65, 115), getmajor.getPreferredSize()));

            //---- getidtext ----
            getidtext.setBorder(LineBorder.createBlackLineBorder());
            querydialogContentPane.add(getidtext);
            getidtext.setBounds(130, 35, 200, 25);

            //---- getnametext ----
            getnametext.setBorder(LineBorder.createBlackLineBorder());
            querydialogContentPane.add(getnametext);
            getnametext.setBounds(130, 75, 200, 25);

            //---- getmajortext ----
            getmajortext.setBorder(LineBorder.createBlackLineBorder());
            querydialogContentPane.add(getmajortext);
            getmajortext.setBounds(130, 115, 200, 25);

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < querydialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = querydialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = querydialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                querydialogContentPane.setMinimumSize(preferredSize);
                querydialogContentPane.setPreferredSize(preferredSize);
            }
            querydialog.setSize(430, 200);
            querydialog.setLocationRelativeTo(querydialog.getOwner());
        }

        //======== nostudentdialog ========
        {
            nostudentdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            nostudentdialog.setAlwaysOnTop(true);
            nostudentdialog.setModal(true);
            var nostudentdialogContentPane = nostudentdialog.getContentPane();
            nostudentdialogContentPane.setLayout(null);

            //---- nostudenttext ----
            nostudenttext.setText("\u8be5\u5b66\u751f\u4e0d\u5b58\u5728\uff01");
            nostudenttext.setFont(nostudenttext.getFont().deriveFont(nostudenttext.getFont().getSize() + 7f));
            nostudentdialogContentPane.add(nostudenttext);
            nostudenttext.setBounds(new Rectangle(new Point(45, 35), nostudenttext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < nostudentdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = nostudentdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = nostudentdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                nostudentdialogContentPane.setMinimumSize(preferredSize);
                nostudentdialogContentPane.setPreferredSize(preferredSize);
            }
            nostudentdialog.setSize(265, 130);
            nostudentdialog.setLocationRelativeTo(nostudentdialog.getOwner());
        }

        //======== adddialog ========
        {
            adddialog.setAlwaysOnTop(true);
            adddialog.setModal(true);
            adddialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            var adddialogContentPane = adddialog.getContentPane();
            adddialogContentPane.setLayout(null);

            //---- addid ----
            addid.setText("\u5b66\u53f7\uff1a");
            addid.setFont(addid.getFont().deriveFont(addid.getFont().getSize() + 8f));
            adddialogContentPane.add(addid);
            addid.setBounds(new Rectangle(new Point(65, 35), addid.getPreferredSize()));

            //---- addname ----
            addname.setText("\u59d3\u540d\uff1a");
            addname.setFont(addname.getFont().deriveFont(addname.getFont().getSize() + 8f));
            adddialogContentPane.add(addname);
            addname.setBounds(new Rectangle(new Point(65, 75), addname.getPreferredSize()));

            //---- addmajor ----
            addmajor.setText("\u4e13\u4e1a\uff1a");
            addmajor.setFont(addmajor.getFont().deriveFont(addmajor.getFont().getSize() + 8f));
            adddialogContentPane.add(addmajor);
            addmajor.setBounds(new Rectangle(new Point(65, 115), addmajor.getPreferredSize()));
            adddialogContentPane.add(addidtext);
            addidtext.setBounds(125, 35, 235, 25);
            adddialogContentPane.add(addnametext);
            addnametext.setBounds(125, 75, 235, 25);
            adddialogContentPane.add(addmajortext);
            addmajortext.setBounds(125, 115, 235, 25);

            //---- confirmbutton ----
            confirmbutton.setText("\u786e\u8ba4");
            confirmbutton.setFont(confirmbutton.getFont().deriveFont(confirmbutton.getFont().getSize() + 9f));
            confirmbutton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    try {
confirmbuttonMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
}
                }
            });
            adddialogContentPane.add(confirmbutton);
            confirmbutton.setBounds(new Rectangle(new Point(180, 160), confirmbutton.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < adddialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = adddialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = adddialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                adddialogContentPane.setMinimumSize(preferredSize);
                adddialogContentPane.setPreferredSize(preferredSize);
            }
            adddialog.setSize(445, 250);
            adddialog.setLocationRelativeTo(adddialog.getOwner());
        }

        //======== existstudentdialog ========
        {
            existstudentdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            existstudentdialog.setAlwaysOnTop(true);
            existstudentdialog.setModal(true);
            var existstudentdialogContentPane = existstudentdialog.getContentPane();
            existstudentdialogContentPane.setLayout(null);

            //---- existstudenttext ----
            existstudenttext.setText("\u8be5\u5b66\u751f\u5df2\u5b58\u5728\uff01");
            existstudenttext.setFont(existstudenttext.getFont().deriveFont(existstudenttext.getFont().getSize() + 7f));
            existstudentdialogContentPane.add(existstudenttext);
            existstudenttext.setBounds(new Rectangle(new Point(45, 35), existstudenttext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < existstudentdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = existstudentdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = existstudentdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                existstudentdialogContentPane.setMinimumSize(preferredSize);
                existstudentdialogContentPane.setPreferredSize(preferredSize);
            }
            existstudentdialog.setSize(265, 130);
            existstudentdialog.setLocationRelativeTo(existstudentdialog.getOwner());
        }

        //======== addokdialog ========
        {
            addokdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            addokdialog.setAlwaysOnTop(true);
            addokdialog.setModal(true);
            var addokdialogContentPane = addokdialog.getContentPane();
            addokdialogContentPane.setLayout(null);

            //---- addokbutton ----
            addokbutton.setText("\u6dfb\u52a0\u6210\u529f");
            addokbutton.setFont(addokbutton.getFont().deriveFont(addokbutton.getFont().getSize() + 7f));
            addokbutton.setFocusPainted(false);
            addokbutton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    try {
addokbuttonMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
}
                }
            });
            addokdialogContentPane.add(addokbutton);
            addokbutton.setBounds(new Rectangle(new Point(30, 35), addokbutton.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < addokdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = addokdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = addokdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                addokdialogContentPane.setMinimumSize(preferredSize);
                addokdialogContentPane.setPreferredSize(preferredSize);
            }
            addokdialog.setSize(185, 130);
            addokdialog.setLocationRelativeTo(addokdialog.getOwner());
        }

        //======== deleteokdialog ========
        {
            deleteokdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            deleteokdialog.setAlwaysOnTop(true);
            deleteokdialog.setModal(true);
            var deleteokdialogContentPane = deleteokdialog.getContentPane();
            deleteokdialogContentPane.setLayout(null);

            //---- deleteokbutton ----
            deleteokbutton.setText("\u5220\u9664\u6210\u529f");
            deleteokbutton.setFont(deleteokbutton.getFont().deriveFont(deleteokbutton.getFont().getSize() + 7f));
            deleteokbutton.setFocusPainted(false);
            deleteokbutton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    try {
deleteokbuttonMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
}
                }
            });
            deleteokdialogContentPane.add(deleteokbutton);
            deleteokbutton.setBounds(new Rectangle(new Point(30, 35), deleteokbutton.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < deleteokdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = deleteokdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = deleteokdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                deleteokdialogContentPane.setMinimumSize(preferredSize);
                deleteokdialogContentPane.setPreferredSize(preferredSize);
            }
            deleteokdialog.setSize(185, 130);
            deleteokdialog.setLocationRelativeTo(deleteokdialog.getOwner());
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JLabel systemname;
    private JScrollPane title;
    private JTable table;
    private JButton query;
    private JButton add;
    private JButton delete;
    private JTextField inputidquery;
    private JButton exit;
    private JTextField inputiddelete;
    private JDialog querydialog;
    private JLabel getid;
    private JLabel getname;
    private JLabel getmajor;
    private JLabel getidtext;
    private JLabel getnametext;
    private JLabel getmajortext;
    private JDialog nostudentdialog;
    private JLabel nostudenttext;
    private JDialog adddialog;
    private JLabel addid;
    private JLabel addname;
    private JLabel addmajor;
    private JTextField addidtext;
    private JTextField addnametext;
    private JTextField addmajortext;
    private JButton confirmbutton;
    private JDialog existstudentdialog;
    private JLabel existstudenttext;
    private JDialog addokdialog;
    private JButton addokbutton;
    private JDialog deleteokdialog;
    private JButton deleteokbutton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on

    public void studentQuery() throws SQLException {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        ResultSet resultSet = StudentSystem.getAllStudent();
        while (resultSet.next()) {
            model.addRow(new Object[]{resultSet.getString(1), resultSet.getString(2), resultSet.getString(3)});
        }
    }

    public void studentAdd(String idIn, String nameIn, String majorIn) throws SQLException {
        if (StudentSystem.contains(idIn)) {
            existstudentdialog.setVisible(true);
            return;
        }

        Student student = new Student(idIn, nameIn, majorIn);

        StudentSystem.addStudent(student);
    }
}
