/*
 * Created by JFormDesigner on Thu Apr 04 10:18:53 CST 2024
 */

package UI;

import StuInfoManageSystem.UserSystem;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;

/**
 * @author 87948
 */
public class FindPasswdFrame extends JFrame {
    public FindPasswdFrame() {
        initComponents();
        this.setVisible(true);
    }

    private void exitMouseReleased(MouseEvent e) {
        // TODO add your code here
        new LoginFrame();
        this.dispose();
    }

    private void confirmMouseReleased(MouseEvent e) throws SQLException {
        // TODO add your code here
        userRecover(inputusername.getText(), inputpersonid.getText(), inputphonenum.getText(), new String(inputresetpassword.getPassword()), new String(inputpasswrodagain.getPassword()));
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        Title = new JLabel();
        usrname = new JLabel();
        resetpassword = new JLabel();
        inputusername = new JTextField();
        inputresetpassword = new JPasswordField();
        confirm = new JButton();
        personid = new JLabel();
        passwordagain = new JLabel();
        inputpasswrodagain = new JPasswordField();
        inputpersonid = new JTextField();
        phonenum = new JLabel();
        inputphonenum = new JTextField();
        exit = new JButton();
        nouserdialog = new JDialog();
        nousertext = new JLabel();
        errorpersoniddialog = new JDialog();
        errorpersonidtext = new JLabel();
        errorphonenumdialog = new JDialog();
        errorphonenumtext = new JLabel();
        differentpassworddialog = new JDialog();
        differentpasswordtext = new JLabel();
        okdialog = new JDialog();
        oktext = new JLabel();

        //======== this ========
        setFont(new Font(Font.DIALOG, Font.PLAIN, 14));
        setAlwaysOnTop(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- Title ----
        Title.setText("\u5b66\u751f\u4fe1\u606f\u7ba1\u7406\u7cfb\u7edf");
        Title.setFont(Title.getFont().deriveFont(Title.getFont().getStyle() & ~Font.ITALIC, Title.getFont().getSize() + 15f));
        contentPane.add(Title);
        Title.setBounds(new Rectangle(new Point(130, 40), Title.getPreferredSize()));

        //---- usrname ----
        usrname.setText("\u7528\u6237\u540d\uff1a");
        usrname.setFont(usrname.getFont().deriveFont(usrname.getFont().getSize() + 6f));
        contentPane.add(usrname);
        usrname.setBounds(new Rectangle(new Point(70, 95), usrname.getPreferredSize()));

        //---- resetpassword ----
        resetpassword.setText("\u91cd\u7f6e\u5bc6\u7801\uff1a");
        resetpassword.setFont(resetpassword.getFont().deriveFont(resetpassword.getFont().getSize() + 6f));
        contentPane.add(resetpassword);
        resetpassword.setBounds(new Rectangle(new Point(50, 215), resetpassword.getPreferredSize()));
        contentPane.add(inputusername);
        inputusername.setBounds(145, 95, 230, 25);
        contentPane.add(inputresetpassword);
        inputresetpassword.setBounds(145, 215, 230, 25);

        //---- confirm ----
        confirm.setText("\u786e\u5b9a");
        confirm.setFont(confirm.getFont().deriveFont(Font.BOLD, confirm.getFont().getSize() + 6f));
        confirm.setFocusPainted(false);
        confirm.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                try {
confirmMouseReleased(e);} catch (SQLException ex) {
    throw new RuntimeException(ex);
}
            }
        });
        contentPane.add(confirm);
        confirm.setBounds(new Rectangle(new Point(220, 310), confirm.getPreferredSize()));

        //---- personid ----
        personid.setText("\u8eab\u4efd\u8bc1\u53f7\uff1a");
        personid.setFont(personid.getFont().deriveFont(personid.getFont().getSize() + 6f));
        contentPane.add(personid);
        personid.setBounds(new Rectangle(new Point(50, 135), personid.getPreferredSize()));

        //---- passwordagain ----
        passwordagain.setText("\u786e\u8ba4\u5bc6\u7801\uff1a");
        passwordagain.setFont(passwordagain.getFont().deriveFont(passwordagain.getFont().getSize() + 6f));
        contentPane.add(passwordagain);
        passwordagain.setBounds(new Rectangle(new Point(50, 255), passwordagain.getPreferredSize()));
        contentPane.add(inputpasswrodagain);
        inputpasswrodagain.setBounds(145, 255, 230, 25);
        contentPane.add(inputpersonid);
        inputpersonid.setBounds(145, 135, 230, 25);

        //---- phonenum ----
        phonenum.setText("\u624b\u673a\u53f7\uff1a");
        phonenum.setFont(phonenum.getFont().deriveFont(phonenum.getFont().getSize() + 6f));
        contentPane.add(phonenum);
        phonenum.setBounds(new Rectangle(new Point(65, 175), phonenum.getPreferredSize()));
        contentPane.add(inputphonenum);
        inputphonenum.setBounds(145, 175, 230, 25);

        //---- exit ----
        exit.setText("\u8fd4\u56de");
        exit.setFont(exit.getFont().deriveFont(Font.BOLD, exit.getFont().getSize() + 6f));
        exit.setFocusPainted(false);
        exit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                exitMouseReleased(e);
            }
        });
        contentPane.add(exit);
        exit.setBounds(new Rectangle(new Point(220, 350), exit.getPreferredSize()));

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(520, 435);
        setLocationRelativeTo(getOwner());

        //======== nouserdialog ========
        {
            nouserdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            nouserdialog.setAlwaysOnTop(true);
            nouserdialog.setModal(true);
            var nouserdialogContentPane = nouserdialog.getContentPane();
            nouserdialogContentPane.setLayout(null);

            //---- nousertext ----
            nousertext.setText("\u5f53\u524d\u7528\u6237\u4e0d\u5b58\u5728\uff01\u8bf7\u5148\u6ce8\u518c");
            nousertext.setFont(nousertext.getFont().deriveFont(nousertext.getFont().getSize() + 7f));
            nouserdialogContentPane.add(nousertext);
            nousertext.setBounds(new Rectangle(new Point(45, 35), nousertext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < nouserdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = nouserdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = nouserdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                nouserdialogContentPane.setMinimumSize(preferredSize);
                nouserdialogContentPane.setPreferredSize(preferredSize);
            }
            nouserdialog.setSize(345, 130);
            nouserdialog.setLocationRelativeTo(nouserdialog.getOwner());
        }

        //======== errorpersoniddialog ========
        {
            errorpersoniddialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            errorpersoniddialog.setAlwaysOnTop(true);
            errorpersoniddialog.setModal(true);
            var errorpersoniddialogContentPane = errorpersoniddialog.getContentPane();
            errorpersoniddialogContentPane.setLayout(null);

            //---- errorpersonidtext ----
            errorpersonidtext.setText("\u8eab\u4efd\u8bc1\u65e0\u6cd5\u5339\u914d\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            errorpersonidtext.setFont(errorpersonidtext.getFont().deriveFont(errorpersonidtext.getFont().getSize() + 7f));
            errorpersoniddialogContentPane.add(errorpersonidtext);
            errorpersonidtext.setBounds(new Rectangle(new Point(45, 35), errorpersonidtext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < errorpersoniddialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = errorpersoniddialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = errorpersoniddialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                errorpersoniddialogContentPane.setMinimumSize(preferredSize);
                errorpersoniddialogContentPane.setPreferredSize(preferredSize);
            }
            errorpersoniddialog.setSize(345, 130);
            errorpersoniddialog.setLocationRelativeTo(errorpersoniddialog.getOwner());
        }

        //======== errorphonenumdialog ========
        {
            errorphonenumdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            errorphonenumdialog.setAlwaysOnTop(true);
            errorphonenumdialog.setModal(true);
            var errorphonenumdialogContentPane = errorphonenumdialog.getContentPane();
            errorphonenumdialogContentPane.setLayout(null);

            //---- errorphonenumtext ----
            errorphonenumtext.setText("\u624b\u673a\u53f7\u65e0\u6cd5\u5339\u914d\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            errorphonenumtext.setFont(errorphonenumtext.getFont().deriveFont(errorphonenumtext.getFont().getSize() + 7f));
            errorphonenumdialogContentPane.add(errorphonenumtext);
            errorphonenumtext.setBounds(new Rectangle(new Point(45, 35), errorphonenumtext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < errorphonenumdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = errorphonenumdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = errorphonenumdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                errorphonenumdialogContentPane.setMinimumSize(preferredSize);
                errorphonenumdialogContentPane.setPreferredSize(preferredSize);
            }
            errorphonenumdialog.setSize(345, 130);
            errorphonenumdialog.setLocationRelativeTo(errorphonenumdialog.getOwner());
        }

        //======== differentpassworddialog ========
        {
            differentpassworddialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            differentpassworddialog.setAlwaysOnTop(true);
            differentpassworddialog.setModal(true);
            var differentpassworddialogContentPane = differentpassworddialog.getContentPane();
            differentpassworddialogContentPane.setLayout(null);

            //---- differentpasswordtext ----
            differentpasswordtext.setText("\u4e24\u6b21\u5bc6\u7801\u4e0d\u4e00\u81f4\uff01\u8bf7\u91cd\u65b0\u8f93\u5165");
            differentpasswordtext.setFont(differentpasswordtext.getFont().deriveFont(differentpasswordtext.getFont().getSize() + 7f));
            differentpassworddialogContentPane.add(differentpasswordtext);
            differentpasswordtext.setBounds(new Rectangle(new Point(45, 35), differentpasswordtext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < differentpassworddialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = differentpassworddialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = differentpassworddialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                differentpassworddialogContentPane.setMinimumSize(preferredSize);
                differentpassworddialogContentPane.setPreferredSize(preferredSize);
            }
            differentpassworddialog.setSize(345, 130);
            differentpassworddialog.setLocationRelativeTo(differentpassworddialog.getOwner());
        }

        //======== okdialog ========
        {
            okdialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            okdialog.setAlwaysOnTop(true);
            okdialog.setModal(true);
            var okdialogContentPane = okdialog.getContentPane();
            okdialogContentPane.setLayout(null);

            //---- oktext ----
            oktext.setText("\u5bc6\u7801\u91cd\u7f6e\u6210\u529f\uff01\u8bf7\u8fd4\u56de\u767b\u5f55");
            oktext.setFont(oktext.getFont().deriveFont(oktext.getFont().getSize() + 7f));
            okdialogContentPane.add(oktext);
            oktext.setBounds(new Rectangle(new Point(45, 35), oktext.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < okdialogContentPane.getComponentCount(); i++) {
                    Rectangle bounds = okdialogContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = okdialogContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                okdialogContentPane.setMinimumSize(preferredSize);
                okdialogContentPane.setPreferredSize(preferredSize);
            }
            okdialog.setSize(345, 130);
            okdialog.setLocationRelativeTo(okdialog.getOwner());
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JLabel Title;
    private JLabel usrname;
    private JLabel resetpassword;
    private JTextField inputusername;
    private JPasswordField inputresetpassword;
    private JButton confirm;
    private JLabel personid;
    private JLabel passwordagain;
    private JPasswordField inputpasswrodagain;
    private JTextField inputpersonid;
    private JLabel phonenum;
    private JTextField inputphonenum;
    private JButton exit;
    private JDialog nouserdialog;
    private JLabel nousertext;
    private JDialog errorpersoniddialog;
    private JLabel errorpersonidtext;
    private JDialog errorphonenumdialog;
    private JLabel errorphonenumtext;
    private JDialog differentpassworddialog;
    private JLabel differentpasswordtext;
    private JDialog okdialog;
    private JLabel oktext;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on

    public void userRecover(String usernameIn, String personidIn, String phonenumIn, String reset, String resetagain) throws SQLException {
        //用户名是否存在
        if (!UserSystem.containsUser(usernameIn)) {
            nouserdialog.setVisible(true);
            return;
        }

        //验证身份证号和手机号
        if (!UserSystem.getUserPersonId(usernameIn).equalsIgnoreCase(personidIn)) {
            errorpersoniddialog.setVisible(true);
            return;
        }
        if (!UserSystem.getUserPhoneNum(usernameIn).equals(phonenumIn)) {
            errorphonenumdialog.setVisible(true);
            return;
        }

        //两次密码输入一致
        if (!resetagain.equals(reset)) {
            differentpassworddialog.setVisible(true);
            return;
        } else {
            UserSystem.updateUserPassword(usernameIn, resetagain);
        }

        UserSystem.updateUserPassword(usernameIn, resetagain);
        okdialog.setVisible(true);
    }
}
