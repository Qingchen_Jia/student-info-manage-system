CREATE SCHEMA if not exists studentinfo;

USE studentinfo;

drop table if exists student;
drop table if exists user;


create table student
(
    Id    varchar(12)                 not null
        primary key,
    name  varchar(12) charset utf8mb3 not null,
    major varchar(12)                 not null
);

create table user
(
    username varchar(10) not null
        primary key,
    password varchar(10) not null,
    personId varchar(18) not null,
    phoneNum varchar(11) not null
);